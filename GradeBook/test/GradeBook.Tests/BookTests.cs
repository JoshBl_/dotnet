using Xunit;
using Xunit.Abstractions;

namespace GradeBook.Tests
{
    public class BookTests
    {
        [Fact]
        public void BookCalculatesStatistics()
        {
            // arrange
            var book = new InMemoryBook("");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.3);

            // act
            var result = book.GetStatistics();
            
            // assert
            Assert.Equal(85.6, result.Average, 1);
            Assert.Equal(90.5, result.High, 1);
            Assert.Equal(77.3, result.Low, 1);
            Assert.Equal('B', result.Letter);
        }

        [Fact]
        public void InvalidGradeRejected()
        {
            var book = new InMemoryBook("");
            
            //var result = book.GetStatistics();

            // Assert.Equal(NaN, result.Average, 1);
            // Assert.Equal(90.5, result.High, 1);
            // Assert.Equal(77.3, result.Low, 1);
        }
    }
}
