﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace gameLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Video Game Library";
            Console.WriteLine("Welcome to the Video Game Library!");
            bool activeMenu = true;
            while (activeMenu == true)
            {
                Console.WriteLine("Press the key to run a command!");
                Console.WriteLine("A) Add game to library\nV) View library\nP) Print to file\nQ) Quit");
                string menuChoice = Console.ReadLine();
                // changing user input to upper case
                menuChoice = menuChoice.ToUpper();
                switch (menuChoice)
                {
                    case "A":
                        addGame();
                        break;
                    case "V":
                        displayGames();
                        break;
                    case "P":
                        VideoGame.printToFile();
                        break;
                    case "Q":
                        activeMenu = false;
                        Console.WriteLine("Goodbye!");
                        break;
                    // error handling
                    default:
                        Console.WriteLine("Error! Please try again!");
                        break;
                }
            }
        }

        static void addGame() {
            // getting input from the user for each field
            Console.WriteLine("Enter the game name");
            string gameName = Console.ReadLine();
            Console.WriteLine("Enter the developer of the game");
            string gameDeveloper = Console.ReadLine();
            Console.WriteLine("Enter the name of the publisher of the game");
            string gamePublisher = Console.ReadLine();
            Console.WriteLine("Enter the name of the platform of the game");
            string gamePlatform = Console.ReadLine();
            Console.WriteLine("Enter the year the game came out");
            double gameYear = Convert.ToDouble(Console.ReadLine());
            // calling the method from the VideoGame class to add values to list
            VideoGame.addGameToList(gameName, gameDeveloper, gamePublisher, gamePlatform, gameYear);
            // confirmation message
            Console.WriteLine(gameName + " has been added!");
        }

        static void displayGames() {
            VideoGame.displayLibrary();
        }
    }

    // new class
    public class VideoGame
    {
        // list that is part of the class
        public static List<VideoGame> library = new List<VideoGame>();
        // variables with get and set
        private string name {get; set;}
        private string developer {get; set;}
        private string publisher {get; set;}
        private string platform {get; set;}
        private double releaseYear {get; set;}
        private double gameId {get; set;}

        // methods to add to list
        public static void addGameToList(string gameName, string dev, string pub, string plat, double year) {
            Console.WriteLine("You have entered " + gameName);
            Console.WriteLine("Adding game to list!");
            double counter = 0;
            // counting each game in the library to ensure that the game ID is unique
            foreach (var game in library) {
                counter++;
            }
            // adding a new instance of the VideoGame class to the library
            library.Add(new VideoGame {gameId = counter, name = gameName, developer = dev, publisher = pub, platform = plat, releaseYear = year });
        }

        public static void displayLibrary() {
            // displaying all the items in the library
            foreach (var game in library) {
                Console.WriteLine(game);
            }
        }

        public static void printToFile() {
            // boolean variable
            bool printLoop = true;
            // while the boolean is true, run the following
            while (printLoop == true)
            {
                Console.WriteLine("Please enter the path to where you want the file to be written to:");
                // creating the path for where the file needs to be written to
                string path = Console.ReadLine();
                path = path + "\\library.txt";
                // if the file already exists, display message and exit method
                if (File.Exists(path))
                {
                    Console.WriteLine("This file already exists! Please try again!");
                }
                // if it doesn't exist, run the following
                else
                {
                    // error handling on writing file
                    try
                    {
                        // empty string to store every item in the library
                        string gameString = "";
                        // iterate through the the game list
                        foreach (VideoGame game in VideoGame.library)
                        {
                            // add each game to the string and add a new line on the end
                            gameString += game + "\n";
                        }

                        // write the file to the path location
                        File.WriteAllText(path, gameString);

                        Console.WriteLine("File written to: " + path);
                        Console.WriteLine("File is called 'library.txt'");
                        // set boolean to false so loop can end
                        printLoop = false;
                        Console.WriteLine("Press enter to return to main menu.");
                        Console.ReadLine();
                    }
                    // error handling
                    catch (Exception e)
                    {
                        Console.WriteLine("Error! Please try again!");
                        Console.WriteLine(e);
                    }
                }
            }
        }

        //this method is used to print items on the list to the screen
        //remember - this override method cannot use the new, static or virtual modifiers 
        //provides a new implementation of a member that is inherited from a base class
        //in this case, we are overriding the default ToString method!
        public override string ToString()
        {
            return "Game ID: " + gameId + "\nGame: " + name + "\nDeveloper: " + developer + "\nPublisher: " + publisher + "\nPlatform: " + platform + "\nYear: " + releaseYear +"\n";
        }
    }
}
