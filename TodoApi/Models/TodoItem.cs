// A model is a set of classes that represent the data that the app manages. The model for this app is a single TodoItem class.
// model classes can go anywhere - but the Models folder is used by convention

namespace TodoApi.Models {
    public class TodoItem {
        // properties defined
        // Id acts as the unique key in a relational database
        public long Id {get; set;}
        public string Name {get; set;}
        public bool isComplete {get; set;}
    }
}