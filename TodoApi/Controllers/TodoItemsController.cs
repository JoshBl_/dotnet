using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    // The HTTP GET attribute denotes a method that responds to an HTTP GET request, the URL path for each method is as follows:
    // start with the template string in the controllers Route attribute
    // replace controller with the name of the controller - which is the class name minus controller (i.e. TodoItemsController)
    // .NET Core routing is case sensitive
    // If the HTTP GET attribute has a route template - append that to the path
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemsController : ControllerBase
    {
        private readonly TodoContext _context;

        public TodoItemsController(TodoContext context)
        {
            _context = context;
        }

        // GET: api/TodoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems()
        {
            return await _context.TodoItems.ToListAsync();
        }

        // GET: api/TodoItems/5
        // id is a placeholder value for the unique identifier of the TodoItem
        [HttpGet("{id}")]
        // the return type for GetTodoItems and GetTodoItem methods is ActionResult
        // ASP .NET Core automatically serialises the object to JSON and writes the JSON into the body of the response message - which will return 200
        // if there are any unhandled exceptions are 5xx errors
        // if no item matches the requested id, return a 404 
        public async Task<ActionResult<TodoItem>> GetTodoItem(long id)
        {
            var todoItem = await _context.TodoItems.FindAsync(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        // PUT: api/TodoItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, TodoItem todoItem)
        {
            if (id != todoItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(todoItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TodoItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
        {
            // this method gets the value of the to-do item from the body of the HTTP request
            _context.TodoItems.Add(todoItem);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
            // this will now use the nameof operator - which obtains the name of a variable, type or member as the string constant
            return CreatedAtAction(nameof(GetTodoItem), new { id = todoItem.Id}, todoItem);
            // CreatedAtAction:
            // returns a 201 if successful, standard response that creates a new resource on the server
            // adds a location header to the response, the header specifies the URI of the newly created to-do item.
            // references the GetTodoItem action to create the Location header URI
            // nameof is used to avoid hard coding the action name in the CreatedAtAction call
        }

        // DELETE: api/TodoItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
        {
            var todoItem = await _context.TodoItems.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            _context.TodoItems.Remove(todoItem);
            await _context.SaveChangesAsync();

            return todoItem;
        }

        private bool TodoItemExists(long id)
        {
            return _context.TodoItems.Any(e => e.Id == id);
        }
    }
}
