﻿using System;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = ("My first .NET Core application");
            Console.WriteLine("Hello World!");
            Console.WriteLine("The current time is " + DateTime.Now);
            Console.WriteLine("What is your name?");
            string name = Console.ReadLine();
            Console.WriteLine("Ah! Your name is " + name);
            Console.WriteLine("How old are you?");
            double age = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("You are " + age  + " years old");
            if (age < 30)
            {
                Console.WriteLine("You're young!");
            } else {
                Console.WriteLine("You're old!");
            }
            testMethod();

        }

        public static void testMethod() {
            Console.WriteLine("Hello - I'm from a method!");
        }
    }
}